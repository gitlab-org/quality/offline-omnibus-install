# offline-omnibus-install

The aim of this project is to test the Omnibus installer when run in an offline environment. It will achieve this by 
downloading all required dependencies and then running the Omnibus installer inside a Docker container that has no 
networking.

The compose file will create a Ubuntu container that has all the dependencies required to install GitLab, it will also 
have the latest nightly build of Gitlab-ee. This container only has an internal network and is unable to access any 
external IP address. The compose file will also create an Nginx server that is connected  to the internal and external 
networks. This allows the GitLab instance to be accessible on port 80 via a proxy_pass handled in Nginx.
Once the containers are built the `configure` script will copy across the GitLab config and run the reconfigure 
command.

## Set up

To run the smoke tests against the GitLab instance locally you will need to add `gitlab.internal` to you hosts file and 
have it point to your local ip address.

### Manual
1. Run `docker-compose up -d`
    - Add `--build --force-recreate` to force a rebuild
1. Run `./bin/configure`

### Scripts
`./bin/build`
This script will build all containers using --no-cache. This means that all external resources will be downloaded. 
This script will generally only be run the first time after checkout. After that using the rebuild/recreate scripts 
will be more beneficial.

`./bin/rebuild`
This performs the same steps as the build script but will remove the existing containers before doing so. This only 
needs to be run to get the latest code changes.

`./bin/recreate`
This works the same as the rebuild script but without the --no-cache option. This is used to recreate the environment 
and remove any data.

`./bin/configure`
This script will configure and run the reconfigure for GitLab, once complete it will run a health check and ensure that 
GitLab is available on port 80.

## Testing

As part of the `configure` script we are carrying out some basic sanity checks to ensure that the omnibus install has 
completed successfully and that GitLab is available on port 80. This is carried out by first running a health check 
against the GitLab instance, checking the network activity on the machine for any external network calls, and then 
running some basic smoke tests against GitLab to verify it is running as expected.

### Health Check Verification

To run a health check we are hitting some different endpoints and verifying that the response we get back is as 
expected. The endpoints used and expected responses are all taken from the 
[GitLab Health Check Documentation](https://docs.gitlab.com/ee/user/admin_area/monitoring/health_check.html#health).

### Network Verification

To verify the network activity we are using NetStat on the GitLab docker container. Before the installation process is 
started we start run the `startNetworkLogger` script which runs netstat and starts logging all calls to IP addresses. 
To prevent the log file from growing to large all network calls to localhost and the internal docker network are 
ignored. Once the installation is complete we scan the log file for any unknown IP address and flag this as a failure.

### Smoke Testing

To finish of the testing we use the `gitlab-qa` gem to run all tests tagged with `:smoke` to verify that the GitLab 
instance is fundamentally working.  

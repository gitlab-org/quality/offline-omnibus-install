FROM ubuntu:18.04

SHELL ["/bin/sh", "-c"],
ENV LANG=C.UTF-8

# Remove file that prevents apt-get storing deb files in /var/cache/apt/archives
RUN rm -rf /etc/apt/apt.conf.d/docker-clean

# Install dependencies
RUN apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      dpkg-dev \
      ca-certificates \
      openssh-server \
      wget \
      apt-transport-https \
      vim \
      tzdata \
      less \
      iputils-ping \
      curl \
      gnupg \
      debian-archive-keyring \
      iproute2 \
      net-tools \
      ruby

# Add repository for GitLab nightly build and download
RUN curl -L https://packages.gitlab.com/gitlab/nightly-builds/gpgkey | apt-key add -

RUN echo "deb https://packages.gitlab.com/gitlab/nightly-builds/ubuntu/ bionic main" >> /etc/apt/sources.list.d/gitlab_nightly-builds.list
RUN echo "deb-src https://packages.gitlab.com/gitlab/nightly-builds/ubuntu/ bionic main" >> /etc/apt/sources.list.d/gitlab_nightly-builds.list

RUN apt-get update -q && apt-get install --download-only gitlab-ee \
    && cp /var/cache/apt/archives/gitlab-ee* /var/cache/apt/archives/gitlab-ee.deb

COPY assets/ /assets/
RUN chmod +x /assets/healthcheck /assets/startNetworkLogger

CMD tail -f /dev/null